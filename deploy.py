import zipfile

files = [
    'clubs',
    'infra',
    'dtb',
    'players',
    'trivia',
    'scouts',
    '__init__.py',
    'context_managment.py',
    'emails.py',
    'handler.py',
    'http_statuses.py',
    'passwords.py',
    'general_tasks',
    'general_info.py',
    'tasks.py'
]
zipfile.main(['-c', 'archive.zip'] + files)
print('zip created')
