import asyncio
import re, hashlib, uuid, string, random

import context_managment

cm = context_managment.ContextManager.instance()

def validate_syntax(password: str) -> bool:
    result = re.match(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#?!@$%^&*-])[A-Za-z\d[#?!@$%^&*-]{8,32}$", password)
    if not result:
        return False
    return True



def generate_password(length=8) -> str:
    def generator():
        LETTERS = string.ascii_letters
        NUMBERS = string.digits
        PUNCTUATION = '#?!@$%^&*-'
        printable = f'{LETTERS}{NUMBERS}{PUNCTUATION}'
        printable = list(printable)
        random.shuffle(printable)
        random_password = random.choices(printable, k=length)
        random_password = ''.join(random_password)
        return random_password

    password = ''
    while not validate_syntax(password):
        password = generator()
    return password



def encrypt_password(salt: str, password: str) -> str:
    encrypted = hashlib.sha512(salt.encode() + password.encode()).hexdigest()
    return encrypted


def generate_salt():
    return uuid.uuid4().hex


def add_password(prefix, password_item, salt_item):
    loop = cm.get_loop()
    req = [
        cm.put_item(f'{prefix}-user-salt', salt_item),
        cm.put_item(f'{prefix}-user-passwords', password_item)
    ]
    loop.run_until_complete(asyncio.gather(*req))
    return



