import asyncio, uuid, emails
from random import randint
import context_managment, http_statuses, passwords
from clubs.club import Club
from datetime import datetime, timedelta

cm = context_managment.ContextManager.instance()


def generate_id(prefix):
    def rand_num():
        return randint(10000000, 99999999)

    loop = cm.get_loop()
    num = rand_num()
    req = [
        cm.get_item(f'{prefix}-id-email', {'id': 'C' + str(num)})
    ]
    id = loop.run_until_complete(asyncio.gather(*req))
    while id:
        num = rand_num()
        req = [
            cm.get_item(f'{prefix}-id-email', {'id': 'C' + str(num)})
        ]
        id = loop.run_until_complete(asyncio.gather(*req))[0]
    return 'C' + str(num)


def signup(body: dict, prefix: str):
    # recaptcha = body.get("recaptcha")
    # if not recaptcha:
    #    return http_statuses.missing_argument("recaptcha")
    # if not cm.recaptcha_validation(recaptcha):
    #    return http_statuses.invalid_recaptcha()

    body = body.get('data')
    if not body:
        return http_statuses.missing_argument('data')
    if not body.get('pp'):
        return http_statuses.invalid_form_field('pp')
    if not body.get('tos'):
        return http_statuses.invalid_form_field('tos')
    if not body:
        return http_statuses.invalid_form_field('data')
    club = Club()
    for key in ['address', 'birthday', 'city', 'club_name', 'country', 'email', 'first_name', 'gender', 'key',
                'last_name', 'league', 'middle_name', 'passport_id', 'phone', 'pp', 'tos']:
        added = club.set_attr(key, body.get(key))
        if added:
            return added

    data_item = body
    loop = cm.get_loop()
    req = [
        cm.get_item(f'{prefix}-email-id', {'email': data_item['email']})
    ]
    email = loop.run_until_complete(asyncio.gather(*req))[0]
    if email:
        return http_statuses.user_exists()
    id = generate_id(prefix)
    data_item['added'] = datetime.utcnow().strftime("%d/%m/%Y")
    data_item['id'] = id
    data_item['locale'] = 'en'
    data_item['status'] = 'waiting_approval'
    req = [
        cm.put_item(f'{prefix}-club-signup-info', data_item),
        cm.put_item(f'{prefix}-id-email', {'id': id, 'email': data_item['email']}),
        cm.put_item(f'{prefix}-email-id', {'id': id, 'email': data_item['email']}),
    ]
    res = loop.run_until_complete(asyncio.gather(*req))
    return http_statuses.success_form()


def login(body: dict, prefix: str):
    username = body.get('username')
    password = body.get('password')
    club = Club()
    club.password = password
    club.id = username
    loop = cm.get_loop()
    asyncio.set_event_loop(loop)
    req = [
        cm.__get_item(f'{prefix}-user-salt', {'id': club.id}),
        cm.__get_item(f'{prefix}-user-passwords', {'id': club.id}),
        cm.__get_item(f'{prefix}-club-signup-info', {'id': club.id})
    ]
    salt, saved_pass, info = loop.run_until_complete(asyncio.gather(*req))
    salt = salt.get('salt')
    saved_pass = saved_pass.get('password')
    encrypted = passwords.encrypt_password(salt, club.password)
    if encrypted == saved_pass:
        return {**http_statuses.success_login_club(), **info}
    return http_statuses.invalid_password()


def files_uploaded(prefix, id):
    club = Club()
    added = club.set_attr('id', id)
    if added:
        return added
    loop = cm.get_loop()
    req = [
        cm.__get_item(f'{prefix}-club-signup-info', {'id': id}),
    ]
    info = loop.run_until_complete(asyncio.gather(*req))[0]
    info['status'] = 'waiting_approval'
    req = {
        cm.put_item(f'{prefix}-club-signup-info', info)
    }
    res = loop.run_until_complete(*req)
    return http_statuses.success()


if __name__ == '__main__':
    body = {
        'data': {
            'club_name': 'club1',
            'country': 'israelush',
            'city': 'KGCCCCC',
            'league': 'bottom',
            'address': 'street bench',
            'first_name': 'failure',
            'last_name': 'noder',
            'gender': 'male',
            'passport_id_number': '123123123132',
            'birthday': '11/12/1998',
            'phone': '+972 0524625891',
            'email': 'ofekedut86@gmail.com',
            "tos": True,
            "pp": True
        }
    }
    print(signup(body, 'op'))

