import re
from datetime import datetime

import emails
import http_statuses
import passwords


class Phone:
    def __init__(self):
        self.region: str
        self.number: str


class Club:
    def __init__(self):
        self.id = None
        self.email = None
        self.password = None
        self.club_name = None
        self.country = None
        self.city = None
        self.league = None
        self.address = None
        self.first_name = None
        self.middle_name = None
        self.last_name = None
        self.gender = None
        self.passport_id_number = None
        self.birthday = None
        self.phone: Phone = Phone()

    def get_name(self):
        name = self.first_name
        name += f' {self.middle_name}' if self.middle_name is not None else None
        name += f' {self.last_name}'
        return name

    @staticmethod
    def validate_id_syntax(id):
        if id[0] == 'C' and len(id) == 9:
            return True
        return False

    @staticmethod
    def calculate_age(birthday: str):
        born = datetime.strptime(birthday, "%d/%m/%Y")
        today = datetime.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    def set_attr(self, key: str, val: any):
        def set_name():
            if not val:
                return http_statuses.missing_argument(key)
            if len(val) > 30:
                return http_statuses.invalid_form_field(key)
            if len(val) < 2:
                return http_statuses.invalid_form_field(key)
            self.__setattr__(key, val)
            return

        def set_birthday():
            if not val:
                return http_statuses.missing_argument(key)
            if self.calculate_age(birthday=val) < 16:
                return http_statuses.invalid_form_field(key)
            if re.match(r'\d\d/\d\d/\d\d\d\d', val):
                self.__setattr__(key, val)
                return
            return http_statuses.invalid_form_field(key)

        def set_email():
            if not val:
                return http_statuses.missing_argument(key)
            if not emails.validate_email(val):
                return http_statuses.invalid_form_field(key)
            self.__setattr__(key, val)
            return

        def set_phone():
            if not val:
                return http_statuses.missing_argument(key)
            if len(val) > 15:
                return http_statuses.invalid_form_field(key)
            phone = re.split(r'[\s]+', val)
            if len(phone) != 2:
                return http_statuses.invalid_form_field(key)
            region = phone[0]
            number = phone[1]
            if region[0] != '+':
                return http_statuses.invalid_form_field(key)
            if not re.match(r'^[\d]+$', number):
                return http_statuses.invalid_form_field(key)
            self.phone.region = region
            self.phone.number = number
            return

        def set_attr():
            if not val:
                return http_statuses.missing_argument(key)
            self.__setattr__(key, val)
            return

        def set_pass():
            if not val:
                return http_statuses.missing_argument(key)
            if not passwords.validate_syntax(val):
                return http_statuses.invalid_password()
            self.__setattr__(key, val)
            return

        def set_middle_name():
            if not val:
                return
            if len(val) > 30:
                return http_statuses.invalid_form_field(key)
            self.__setattr__(key, val)
            return

        def set_passport():
            if not val:
                return http_statuses.missing_argument(key)
            if len(val) > 16:
                return http_statuses.invalid_form_field(key)

        cases = {
            'club_name': set_name,
            'country': set_attr,
            'league': set_attr,
            'email': set_email,
            'first_name': set_name,
            'middle_name': set_middle_name,
            'last_name': set_name,
            'gender': set_attr,
            'passport_id_number': set_passport,
            'birthday': set_birthday,
            'phone': set_phone,
            'password': set_pass
        }
        if key in cases:
            return cases[key]()
        else:
            self.__setattr__(key, val)
            return
