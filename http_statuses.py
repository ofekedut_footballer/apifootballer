def success():
    return {
        "success": True,
        "statusCode": 200,
        "message": "success"
    }


def missing_argument(argument: str):
    return {
        "statusCode": 400,
        "success": False,
        "message": "ERR_MISSING_FORM_FIELD",
        "data": {
            "field": argument
        }}


def invalid_club_id():
    return {
        "statusCode": 400,
        "message": "ERR_INVALID_CLUB_ID"
    }


def invalid_player_id():
    return {
        "success": False,
        "statusCode": 400,
        "message": "ERR_INVALID_PLAYER_ID"
    }


def invalid_password():
    return {
        "success": False,
        "statusCode": 400,
        "message": "ERR_INVALID_PASSWORD"
    }


def success_login_club():
    return {
        "success": True,
        'message': 'SUCCESS_LOGIN_REDIRECT_CLUB',
        "statusCode": 200,
    }


def success_login_scout():
    return {
        "success": True,
        'message': 'SUCCESS_LOGIN_REDIRECT_SCOUT',
        "statusCode": 200,
    }


def success_login_player():
    return {"success": True,
            'message': 'SUCCESS_LOGIN_REDIRECT_PLAYER',
            "statusCode": 200,
            }


def invalid_email():
    return {
        "success": False,
        "statusCode": 400,
        "message": "ERR_INVALID_EMAIL"
    }


def invalid_scout_id():
    return {
        "success": False,
        "statusCode": 400,
        "message": "ERR_INVALID_SCOUT_ID"
    }


def invalid_recaptcha():
    return {
        "statusCode": 400,
        "success": False,
        "message": "ERR_INVALID_RECAPTCHA_TOKEN"
    }


def user_exists():
    return {
        "success": False,
        "statusCode": 400,
        "message": "ERR_USER_EXISTS"
    }


def invalid_id():
    return {
        "success": False,
        "statusCode": 400,
        "message": "ERR_INVALID_ID"
    }


def invalid_form_field(key):
    return {
        "success": False,
        "statusCode": 400,
        "message": "ERR_INVALID_FORM_FIELD",
        "data": {
            "field": key
        }
    }


def success_form():
    return {
        "statusCode": 200,
        "success": True,
        "message": "OK_FORM_SENT"
    }


def missing_in_system(param):
    return {
        "statusCode": 500,
        "success": True,
        "message": f"ERR_MISSING_IN_SYSTEM{param}"
    }


def invalid_token():
    return {
        "statusCode": 400,
        "success": True,
        "message": "ERR_TOKEN_INVALID"
    }


def valid_token():
    return {
        "statusCode": 200,
        "success": True,
        "message": "OK_TOKEN_VALID"
    }
