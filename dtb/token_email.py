import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class TokenEmailTable:
    NAME = 'token-email'
    SLEEP = 0.05

    @classmethod
    async def __get_item(cls, prefix: str, token: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'token': token})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def get_email(cls, prefix, token):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, token)
        return item.get('email', '')

    @classmethod
    async def delete_token(cls, prefix, token):
        await asyncio.sleep(cls.SLEEP)
        await cm.batch_delete_item(f'{prefix}-{cls.NAME}', [{'token': token}])
        return

    @classmethod
    async def put_token(cls, prefix, token, email, id, expire: int):
        await asyncio.sleep(cls.SLEEP)
        await cls.__update_item(prefix, {'token': token, 'email': email, 'id': id, 'expire': expire})
        return
