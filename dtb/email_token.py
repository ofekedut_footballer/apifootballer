import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class EmailTokenTable:
    NAME = 'email-token'
    SLEEP = 0.05

    @classmethod
    async def __get_item(cls, prefix: str, email: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'email': email})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def set_token(cls, prefix, email, token, id, timestamp: int):
        await asyncio.sleep(cls.SLEEP)
        item = {
            'email': email,
            'token': token,
            'expire': timestamp,
            'id': id
        }
        await cls.__update_item(prefix, item)
        return

    @classmethod
    async def get_token(cls, prefix, email):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, email)
        token = item['token']
        return token

    @classmethod
    async def delete_token(cls, prefix, email):
        await asyncio.sleep(cls.SLEEP)
        await cm.batch_delete_item(f'{prefix}-{cls.NAME}', [{'email': email}])
        return