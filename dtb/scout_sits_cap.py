import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class ScoutSitsCapTable:
    NAME = 'scout-sits-cap'
    SLEEP = 0.02

    @classmethod
    async def __get_item(cls, prefix: str, id: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'scout': id})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def get_cap(cls, prefix, scout_id: str):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, scout_id)
        return item['cap'] if item else None
