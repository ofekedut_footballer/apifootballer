import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class IdEmailTable:
    NAME = 'scout-birthday'
    SLEEP = 0.03

    @classmethod
    async def __get_item(cls, prefix: str, id: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'scout': id})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def remove_birthday(cls, prefix, id):
        await asyncio.sleep(cls.SLEEP)
        await cm.batch_delete_item(f'{prefix}-{cls.NAME}', {'scout': id})
        return

    @classmethod
    async def get_birthday(cls, prefix, scout_id):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, scout_id)
        return item['birthday'] if item else ''

    @classmethod
    async def set_birthday(cls, prefix, scout_id, birthday: str):
        await asyncio.sleep(cls.SLEEP)
        item = {
            'birthday': birthday,
            'scout': scout_id
        }
        await cls.__update_item(prefix, item)
        return
