import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class EmailIdTable:
    NAME = 'email-id'
    SLEEP = 0.05

    @classmethod
    async def __get_item(cls, prefix: str, email: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'email': email})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def get_id(cls, prefix, email):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, email)
        return item['id'] if item else {}

    @classmethod
    async def remove_email(cls, prefix, email):
        await asyncio.sleep(cls.SLEEP)
        await cm.batch_delete_item(f'{prefix}-{cls.NAME}', {'email': email})

