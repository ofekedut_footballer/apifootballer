from infra import dynamodb

TABLES = [  # TODO I step: add table name and attributes definitions
    ('user-salt', 'id', 'S'),
    ('user-password', 'id', 'S'),
    ('email-id', 'email', 'S'),
    ('id-email', 'id', 'S'),
    ('token-email', 'token', 'S'),
    ('email-token', 'email', 'S'),  # manually set TTL to "expire"

    ('player-signup-info', 'id', 'S'),
    ('scout-signup-info', 'id', 'S'),
    ('club-signup-info', 'id', 'S'),
    # ------------------------ scouts
    ('scout-player', 'scout', 'S', 'player', 'S'),
    ('scout-sits-cap', 'scout', 'S',),
    ('scout-birthday', 'scout', 'S'),
    ('scout-passport-id', 'scout', 'S')
    # ------------------------ /scouts

]

PREFIXES = [
    'op',
    'test'
]


def create_tables():  # TODO II step run script
                      # TODO III step create wrapper
    for prefix in PREFIXES:
        for table in TABLES:
            name = table[0]
            primary_key = table[1]
            primary_key_type = table[2]
            secondary_key = table[3] if len(table) > 3 else None
            secondary_key_type = table[4] if len(table) > 3 else None
            print(dynamodb.create_table(prefix, name, primary_key, primary_key_type, secondary_key, secondary_key_type))
