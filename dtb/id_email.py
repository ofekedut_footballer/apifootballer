import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class IdEmailTable:
    NAME = 'id-email'
    SLEEP = 0.05

    @classmethod
    async def __get_item(cls, prefix: str, id: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'id': id})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def get_token(cls, prefix: str, id: str):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, id)
        return item.get('token', '')

    @classmethod
    async def pop_token(cls, prefix, id):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, id)
        item.pop('token')
        await cls.__update_item(prefix, item)
        return

    @classmethod
    async def update_email(cls, prefix, id, email):
        await asyncio.sleep(cls.SLEEP)
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'id': id})
        item['email'] = email
        await cls.__update_item(prefix, item)
        return

    @classmethod
    async def get_email(cls, prefix, id):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, id)
        return item['email'] if item else {}

    @classmethod
    async def remove_email(cls, prefix, id):
        await asyncio.sleep(cls.SLEEP)
        await cm.batch_delete_item(f'{prefix}-{cls.NAME}', {'id': id})
