import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class ScoutPlayerTable:
    NAME = 'scout-player'
    SLEEP = 0.02

    @classmethod
    async def __get_item(cls, prefix: str, id: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'scout': id})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def __query_scout(cls, prefix, scout_id: str):
        item = await cm.query_item(f'{prefix}-{cls.NAME}', 'scout', scout_id)
        return item

    @classmethod
    async def get_player_list(cls, prefix, scout_id: str):
        await asyncio.sleep(cls.SLEEP)
        data = await cls.__query_scout(prefix, scout_id)
        l = [item['player'] for item in data]
        return l

    @classmethod
    async def set_player(cls, prefix, scout_id: str, player_id: str):
        await asyncio.sleep(cls.SLEEP)
        item = {
            'scout': scout_id,
            'player': player_id
        }
        return await cls.__update_item(prefix, item)

