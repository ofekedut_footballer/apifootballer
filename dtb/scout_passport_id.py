import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class IdEmailTable:
    NAME = 'scout-passport-id'
    SLEEP = 0.05

    @classmethod
    async def __get_item(cls, prefix: str, id: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'scout': id})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def remove_passports_entirely(cls, prefix, id):
        await asyncio.sleep(cls.SLEEP)
        await cm.batch_delete_item(f'{prefix}-{cls.NAME}', {'scout': id})
        return

    @classmethod
    async def set_passports(cls, prefix, passports: list, scout_id: str):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, scout_id)
        for passport in item['passports']:
            if passport not in passports:
                item['passports'].append(passport)
        await cls.__update_item(prefix, item)
        return

    @classmethod
    async def remove_passports(cls, prefix, passports: list, scout_id: str):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, scout_id)
        passports_known = set(item['passports'])
        passports_updated = passports_known.difference(set(passports))
        leftover = set(passports).difference(passports_known)
        if leftover:
            raise Exception("wrong passports remove input")
        item['passports'] = list(passports_updated)
        await cls.__update_item(prefix, item)
        return

    @classmethod
    async def get_passports(cls, prefix, scout_id: str):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, scout_id)
        return item['passports'] if item else []
