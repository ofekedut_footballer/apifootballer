import asyncio
from context_managment import ContextManager

cm = ContextManager.instance()


class IdEmailTable:
    NAME = 'player-id-position'
    SLEEP = 0.03

    @classmethod
    async def __get_item(cls, prefix: str, id: str):
        item = await cm.get_item(f'{prefix}-{cls.NAME}', {'id': id})
        return item if item else {}

    @classmethod
    async def __update_item(cls, prefix, item):
        await cm.put_item(f'{prefix}-{cls.NAME}', item)
        return

    @classmethod
    async def set_primary_pos(cls, prefix: str, id: str, pos: str):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, id)
        item['primary'] = pos
        await cls.__update_item(prefix, item)
        return

    @classmethod
    async def get_primary_pos(cls, prefix: str, id: str):
        await asyncio.sleep(cls.SLEEP)
        item = await cls.__get_item(prefix, id)
        return item['primary'] if item else ''
