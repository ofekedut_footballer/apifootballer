import context_managment
import json
import players.logger as players_logger
import tasks
from general_tasks import account_recovery
from scouts.tasks import scouts_dashboard
from players import tasks as players_tasks

cm = context_managment.ContextManager.instance()

# Level access:
# Each level of access will be defined by a number in a power of 2 (e.g. 1,2,4,8,16,32,etc')
# You sum the level numbers up to know which level access to provide
# The "access number" will be stored in the DB and will be used in order to give permissions
#
# READONLY_LEVEL = 1
# WRITE_LEVEL = 2
# ADMIN_LEVEL = READONLY_LEVEL + WRITE_LEVEL

ROUTES = {
    'GET': {
        'admin': {
            'scouter': {
                'waiting_approval': tasks.scouters_waiting_approval
            },
            'club': {
                'waiting_approval': tasks.clubs_waiting_approval
            },
            'player': {
                'waiting_approval': tasks.player_waiting_approval
            },
            'count': {
                'players': {
                    'signed': players_tasks.count_signed_players.count
                }
            }
        }
    },
    'POST': {
        'auth': {
            'login': tasks.login,
            'reset_pass_email': account_recovery.reset_pass_email,
            'reset_validate_token': account_recovery.reset_validate_token,
            'reset_pass_new': account_recovery.reset_pass_new
        },
        'dashboard': {
            'scouters': {
                'cap': scouts_dashboard.players_counter
            }
        },
        'forms': {
            'new': {
                'scout': tasks.new_scout,
                'club': tasks.new_club,
                'player': {
                    'step_one': players_logger.singup_step1,
                    'approve_email': players_logger.signup_approve_email,
                    'step_two': players_logger.signup_step2,
                    'custodian': players_logger.singup_custodian
                }
            },
            'registration_files_uploaded': tasks.registration_files_uploaded
        },
        'admin': {
            'scouter': {
                'approve': tasks.approve_scouters
            },
            'club': {
                'approve': tasks.approve_clubs
            },
            'player': {
                'approve': tasks.approve_players
            }
        }
    }
}


def get_sns(event):
    body = event.get('body')
    body = event['Records'][0]['Sns']['Message'] if not body else body
    body = json.loads(body) if isinstance(body, str) else body
    return body


def get_post(event):
    body = event.get('body', '')
    body = json.loads(body) if isinstance(body, str) else body
    return body


def get_get(event):
    body = event.get('queryStringParameters')
    body = event.get('query') if not body else None
    return body


def handler(event, context):
    def get_body():
        methods = {
            'sns': get_sns,
            'POST': get_post,
            'GET': get_get
        }
        return methods[method](event)

    print(event)
    method = event.get('Records', [{}])[0].get('Sns', {}).get('Message')
    method = 'sns' if method else None
    if not method:
        method = event.get('httpMethod')
        method = event.get('method', method)
    args = context_managment.Args()
    args.body = get_body()
    route = event.get('path')
    route = route.split('/')
    route = [item for item in route if item != '']
    i = 0
    lenght = len(route)
    current_pos = route[i]
    root = ROUTES[method]
    while not callable(root[current_pos]) and i < lenght - 1:
        root = root[current_pos]
        current_pos = route[i + 1]
        i += 1
    root = root[current_pos]
    try:
        cm.result = root(args)
    except Exception as e:
        return {
            'statusCode': 500,
            'headers': {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            'isBase64Encoded': event.get('isBase64Encoded', False),
            'body': json.dumps(str(e))
        }
    status = cm.result.pop('statusCode', 200)
    Context_Type = cm.result.pop('headers', {}).get('Content-Type', 'application/json')
    data = json.dumps(cm.result) if Context_Type == 'application/json' else cm.result
    response = {
        'statusCode': status,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': Context_Type
        },
        'isBase64Encoded': event.get('isBase64Encoded', False),
        'body': data
    }
    print(response)
    if cm.type == context_managment.ContextTypes.test:
        cm.save_test()
    return response


if __name__ == "__main__":
    event = {'resource': '/{api+}', 'path': '/forms/new/scout', 'httpMethod': 'POST', 'headers': {'accept': '*/*', 'accept-encoding': 'gzip, deflate, br', 'accept-language': 'en-us', 'content-type': 'application/json, application/json, application/json, application/json', 'Host': '68i4yask6b.execute-api.eu-central-1.amazonaws.com', 'origin': 'https://footballer-app-signup.s3.eu-central-1.amazonaws.com', 'referer': 'https://footballer-app-signup.s3.eu-central-1.amazonaws.com/index.html', 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Safari/605.1.15', 'X-Amzn-Trace-Id': 'Root=1-5f4ffb1d-cef6e0408372f52f832acc82', 'x-api-key': 'QsCC0cJ10i5XixwKlfbxG6qM9EeoifWvZmM6Hlq9', 'X-Forwarded-For': '213.57.61.135', 'X-Forwarded-Port': '443', 'X-Forwarded-Proto': 'https'}, 'multiValueHeaders': {'accept': ['*/*'], 'accept-encoding': ['gzip, deflate, br'], 'accept-language': ['en-us'], 'content-type': ['application/json, application/json, application/json, application/json'], 'Host': ['68i4yask6b.execute-api.eu-central-1.amazonaws.com'], 'origin': ['https://footballer-app-signup.s3.eu-central-1.amazonaws.com'], 'referer': ['https://footballer-app-signup.s3.eu-central-1.amazonaws.com/index.html'], 'User-Agent': ['Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Safari/605.1.15'], 'X-Amzn-Trace-Id': ['Root=1-5f4ffb1d-cef6e0408372f52f832acc82'], 'x-api-key': ['QsCC0cJ10i5XixwKlfbxG6qM9EeoifWvZmM6Hlq9'], 'X-Forwarded-For': ['213.57.61.135'], 'X-Forwarded-Port': ['443'], 'X-Forwarded-Proto': ['https']}, 'queryStringParameters': None, 'multiValueQueryStringParameters': None, 'pathParameters': {'api': 'forms/new/scout'}, 'stageVariables': None, 'requestContext': {'resourceId': 'cqwcpt', 'resourcePath': '/{api+}', 'httpMethod': 'POST', 'extendedRequestId': 'SQQsjEu6FiAFR5Q=', 'requestTime': '02/Sep/2020:20:05:49 +0000', 'path': '/dev/forms/new/scout', 'accountId': '643215917486', 'protocol': 'HTTP/1.1', 'stage': 'dev', 'domainPrefix': '68i4yask6b', 'requestTimeEpoch': 1599077149068, 'requestId': '82b6ccdb-e382-461f-9339-979a3678f0f5', 'identity': {'cognitoIdentityPoolId': None, 'cognitoIdentityId': None, 'apiKey': 'QsCC0cJ10i5XixwKlfbxG6qM9EeoifWvZmM6Hlq9', 'principalOrgId': None, 'cognitoAuthenticationType': None, 'userArn': None, 'apiKeyId': 'mt1jsg1o08', 'userAgent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Safari/605.1.15', 'accountId': None, 'caller': None, 'sourceIp': '213.57.61.135', 'accessKey': None, 'cognitoAuthenticationProvider': None, 'user': None}, 'domainName': '68i4yask6b.execute-api.eu-central-1.amazonaws.com', 'apiId': '68i4yask6b'}, 'body': '{"recaptcha_token":null,"data":{"first_name":"ofek","middle_name":"avraham","last_name":"edut","email":"ofekedut86@gmail.com","gender":"male","phone":"+972 0584300111","experience":"none","current_occupation":"none","birthday":"04/12/1998","pp":true,"tos":true}}', 'isBase64Encoded': False}


    print(handler(event, None))
