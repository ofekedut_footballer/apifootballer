import re, requests


def validate_email(email: str):
    try:
        blocked = get_emails_list()
    except:
        blocked = []
        # todo trigger alert sns failed_to_retrive_emails
    if '@' not in email:
        return False
    domain = email.split('@')[1]
    if domain in blocked:
        return False
    return True


def get_emails_list():
    url = 'https://gist.githubusercontent.com/michenriksen/8710649/raw/e09ee253960ec1ff0add4f92b62616ebbe24ab87/disposable-email-provider-domains'
    response = requests.get(url)
    result = response.text
    result = re.split(r'[\n]', result)
    return result
