import context_managment, asyncio
import http_statuses
from context_managment import Args
from scouts.scout import Scout
from dtb import scout_player, scout_sits_cap

cm = context_managment.ContextManager.instance()


def players_counter(args: Args):
    body = args.body
    prefix = args.prefix
    if 'user_id' not in body:
        return http_statuses.missing_argument('user_id')
    id = body.get('user_id')
    id = id.upper()
    if not id:
        return http_statuses.invalid_scout_id()
    scout = Scout()
    error = scout.set_attr('scout_id', id)
    if error:
        return error
    request = [
        scout_player.ScoutPlayerTable.get_player_list(prefix, id),
        scout_sits_cap.ScoutSitsCapTable.get_cap(prefix, id),
    ]
    l, cap = cm.get_loop().run_until_complete(asyncio.gather(*request))
    if not cap:
        return http_statuses.invalid_scout_id()
    count = len(l)
    return {
        "cap": cap,
        "current_amount": count
    }


if __name__ == "__main__":
    args = Args()
    args.prefix = 'op'
    args.body = {
        'user_id': 'S1'
    }
    print(players_counter(args))
