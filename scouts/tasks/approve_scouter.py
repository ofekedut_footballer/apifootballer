import asyncio
import context_managment, http_statuses, passwords

cm = context_managment.ContextManager.instance()


def waiting_approval(prefix):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    req = [
        cm.scan_table(f'{prefix}-scout-signup-info', 'status', 'waiting_approval'),
    ]
    unapproved = loop.run_until_complete(asyncio.gather(*req))[0]
    return{'unapproved': unapproved, **http_statuses.success()}


def approve_scouter(prefix, id):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    req = [
        cm.get_item(f'{prefix}-scout-signup-info', {'id': id}),
    ]
    item = loop.run_until_complete(asyncio.gather(*req))[0]
    item['status'] = 'approved'
    req = [
        cm.put_item(f'{prefix}-scout-signup-info', item),
    ]
    item = loop.run_until_complete(asyncio.gather(*req))[0]
    return {
        'message': 'success'}
