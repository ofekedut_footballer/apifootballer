import asyncio
from random import randint

import context_managment, http_statuses, passwords
from scouts.scout import Scout
from datetime import datetime, timedelta

cm = context_managment.ContextManager.instance()


def generate_id(prefix):
    def rand_num():
        return randint(10000000, 99999999)

    loop = cm.get_loop()
    num = rand_num()
    req = [
        cm.get_item(f'{prefix}-id-email', {'id': 'S' + str(num)})
    ]
    id = loop.run_until_complete(asyncio.gather(*req))
    while id:
        num = rand_num()
        req = [
            cm.get_item(f'{prefix}-id-email', {'id': 'S' + str(num)})
        ]
        id = loop.run_until_complete(asyncio.gather(*req))[0]
    return 'S' + str(num)


def singup(body: dict, prefix: str):
    #recaptcha = body.get("recaptcha_token")
    #if not recaptcha:
    #    return http_statuses.missing_argument('RECAPTCHA')
    #if not cm.recaptcha_validation(recaptcha):
    #    return http_statuses.invalid_recaptcha()
    #body = body.get('data')
    #if not body:
    #    return http_statuses.missing_argument('data')
    body = body.get('data')
    if not body:
        return http_statuses.missing_argument('data')
    if not body.get('tos'):
        return http_statuses.invalid_form_field('tos')
    if not body.get('pp'):
        return http_statuses.invalid_form_field('pp')

    scout = Scout()
    for key in ['birthday', 'current_occupation', 'email',
                'experience', 'first_name', 'gender', 'last_name', 'passport_id', 'phone', 'pp', 'tos']:
        error = scout.set_attr(key, body.get(key))
        if error:
            return error
    if body.get('middle_name'):
        added = scout.set_attr('middle_name', body.get('middle_name'))
        if added:
            print('middle_name')
            return added
    item = body
    loop = cm.get_loop()
    asyncio.set_event_loop(loop)
    req = [
        cm.get_item(f'{prefix}-email-id', {'email': item['email']})
    ]
    email = loop.run_until_complete(asyncio.gather(*req))[0]
    if email:
        return http_statuses.user_exists()
    id = generate_id(prefix)
    item['added'] = datetime.utcnow().strftime("%d/%m/%Y")
    item['id'] = str(id)
    item['locale'] = 'en'
    item['status'] = 'waiting_approval'
    req = [
        cm.put_item(f'{prefix}-scout-signup-info', item),
        cm.put_item(f'{prefix}-id-email', {'id': id, 'email': item['email']}),
        cm.put_item(f'{prefix}-email-id', {'id': id, 'email': item['email']})
    ]
    res = loop.run_until_complete(asyncio.gather(*req))
    return http_statuses.success_form()


def login(body: dict, prefix: str):
    username = body.get('username')
    password = body.get('password')
    scout = Scout()
    if not scout.validate_id_syntax(scout.id):
        return http_statuses.invalid_scout_id()
    scout.password = password
    scout.id = username
    req = [
        cm.__get_item(f'{prefix}-user-salt', {'id': scout.id}),
        cm.__get_item(f'{prefix}-user-password', {'id': scout.id}),
        cm.__get_item(f'{prefix}-scout-signup-info', {'id': scout.id})
    ]
    salt, saved_pass, info = cm.get_loop().run_until_complete(asyncio.gather(*req))
    salt = salt.get('salt')
    saved_pass = saved_pass.get('password')
    encrypted = passwords.encrypt_password(salt, scout.password)
    if encrypted == saved_pass:
        return {**http_statuses.success_login_scout(), **info}
    return http_statuses.invalid_password()


def files_uploaded(prefix, id):
    scout = Scout()
    added = scout.set_attr('id', id)
    loop = cm.get_loop()
    if added:
        return added
    req = [
        cm.__get_item(f'{prefix}-scout-signup-info', {'id': id}),
    ]
    info = loop.run_until_complete(asyncio.gather(*req))[0]
    info['status'] = 'waiting_approval'
    req = {
        cm.put_item(f'{prefix}-scout-signup-info', info)
    }
    res = loop.run_until_complete(*req)
    return http_statuses.success()


if __name__ == '__main__':
    body = {'data': {'address': 'street',
                     'birthday': '04/12/1998',
                     'city': 'kgc',
                     'country': 'israel',
                     'current_occupation': 'programmer',
                     'email': 'ofekedut12@gmail.com',
                     'experience': 'none',
                     'first_name': 'ofek', 'gender': 'male', 'last_name': 'edut', 'passport_id': '12345566', 'phone': '+972 0584300111',
                     'pp': True, 'tos': True}}
    print(singup(body, 'op'))
