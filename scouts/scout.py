import re
from datetime import datetime

import emails
import http_statuses


class Phone:
    def __init__(self):
        self.region: str
        self.number: str


class Scout:

    def __init__(self):
        self.first_name: str = ''
        self.last_name: str = ''
        self.middle_name: str = ''
        self.id: str = ''
        self.email: str = ''
        self.password: str = ''
        self.phone = Phone()
        self.country: str = ''
        self.city: str = ''
        self.address: str = ''
        self.birthday: str = ''
        self.experience = ''
        self.current_occupation: str = ''
        self.gender: str = ''

    @staticmethod
    def calculate_age(birthday: str):
        born = datetime.strptime(birthday, "%d/%m/%Y")
        today = datetime.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    def set_attr(self, key: str, val: any):

        def set_name():
            if not val:
                return http_statuses.missing_argument(key)
            if key == 'first_name' and len(val) < 2:
                return http_statuses.invalid_form_field(key)
            if len(key) > 30:
                return http_statuses.invalid_form_field(key)
            self.__setattr__(key, val)
            return

        def set_birthday():
            if not val:
                return http_statuses.missing_argument(key)
            if self.calculate_age(val) < 16:
                return http_statuses.missing_argument(key)
            if re.match(r'\d\d/\d\d/\d\d\d\d', val):
                self.__setattr__(key, val)
                return
            return http_statuses.invalid_form_field(key)

        def set_email():
            if not val:
                return http_statuses.missing_argument(key)
            if not emails.validate_email(val):
                return http_statuses.invalid_form_field(key)
            self.__setattr__(key, val)
            return

        def set_phone():
            if not val:
                return http_statuses.missing_argument(key)
            if len(val) > 15:
                return http_statuses.invalid_form_field(key)
            phone = re.split(r'[\s]+', val)
            if len(phone) != 2:
                return http_statuses.invalid_form_field(key)
            region = phone[0]
            number = phone[1]
            if region[0] != '+':
                return http_statuses.invalid_form_field(key)
            if not re.match(r'^[\d]+$', number):
                return http_statuses.invalid_form_field(key)
            self.phone.region = region
            self.phone.number = number
            return

        def set_id():
            if not val:
                return http_statuses.missing_argument(key)
            if not self.validate_id_syntax(val):
                return http_statuses.invalid_scout_id()
            self.__setattr__(key, val)
            return

        def set_attr():
            if val:
                self.__setattr__(key, val)
                return
            return http_statuses.invalid_form_field(key)

        def set_gender():
            if val.lower() not in ['male', 'female', 'other']:
                return http_statuses.invalid_form_field('gender')
            self.__setattr__(key, val)
            return

        cases = {
            'scout_id': set_id,
            'first_name': set_name,
            'middle_name': set_name,
            'last_name': set_name,
            'birthday': set_birthday,
            'email': set_email,
            'phone': set_phone,
            'experience': set_attr,
            'current_occupation': set_attr,
            'country': set_attr,
            'city': set_attr,
            'address': set_attr,
            'gender': set_gender
        }
        if key in cases:
            return cases[key]()
        else:
            self.__setattr__(key, val)
            return

    @classmethod
    def validate_id_syntax(cls, id):
        if id[0] != 'S':
            return {}
        if not re.match(r'^[0-9]+$', id[1:]):
            return {}
        return True
