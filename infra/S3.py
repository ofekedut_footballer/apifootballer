import decimal, boto3, json
from infra import aws

s3 = aws.get_session()

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return super(DecimalEncoder, self).default(o)


def pub_obj(bucket_name: str, obj_name: str, data: dict):
    s3 = boto3.resource('s3')
    obj = s3.Object(bucket_name, obj_name)
    obj.put(
        Body=(bytes(json.dumps(data, cls=DecimalEncoder).encode('UTF-8')))
    )
