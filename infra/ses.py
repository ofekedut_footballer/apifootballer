import boto3
from botocore.exceptions import ClientError
from infra import aws


FROM_ADDRESS = 'no-reply@footballeragency.com'
SENDER = f'Footballer Agency <{FROM_ADDRESS}>'
CHARSET = 'UTF-8'
def get_ses():
    return aws.get_ses()


def send_email(recepient: str, subject: str, body_html, body_reg, verification_print: bool = False):
    RECIPIENT = recepient
    client = get_ses()
    try:
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': body_html,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': body_reg,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': subject,
                },
            },
            Source=SENDER,
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        if verification_print:
            print("Email sent! Message ID:"),
            print(response['MessageId'])
    return