import json, requests
from infra import credentials

def validate_recaptcha(recaptcha_token: str):
    url = 'https://www.google.com/recaptcha/api/siteverify'
    payload = {
        'secret': credentials.get_google_recaptcha_secret(),
        'response': recaptcha_token
    }
    response = requests.request('POST', url, data=payload)
    result = response.text
    data = json.loads(result)
    success = data['success']
    if success:
        return True
    else:
        return False

