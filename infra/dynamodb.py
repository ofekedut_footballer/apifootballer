from boto3.dynamodb.conditions import Key, Attr
from infra import aws
import asyncio


def get_dynamo():
    """
    sets up dynamodb session
    :return session:
    """
    session = aws.get_session()
    return session.resource('dynamodb')


def put_item(table_name: str, item: dict) -> dict:
    """
    :param item: the item to put/update
    """
    dynamo = get_dynamo()
    table = dynamo.Table(table_name)
    return table.put_item(Item=item)


def get_item(table_name: str, key: dict) -> dict:
    """
    :param key: item key like {"key" : key, "range_key": range_key} or {"key" : key}
    :return: the item requested or {} when not found
    """
    dynamo = get_dynamo()
    table = dynamo.Table(table_name)
    table_item = table.get_item(Key=key)
    item = table_item.get('Item')
    return item


def batch_get_item(table_name: str, keys: list) -> list:
    """
    :param keys: a list of keys list[dict]
    :return: the items requested or [] when none were found
    """
    if not keys:
        return []
    dynamodb = get_dynamo()
    result = []
    keys_to_process = []
    for key in keys:
        if key not in keys_to_process:
            keys_to_process.append(key)
    leftovers = []
    while leftovers or keys_to_process:
        keys_to_take = min(100 - len(leftovers), len(keys_to_process))
        curr_keys = leftovers + keys_to_process[:keys_to_take]
        keys_to_process = keys_to_process[keys_to_take:]
        response = dynamodb.batch_get_item(
            RequestItems={
                table_name: {
                    'Keys': curr_keys
                }
            }
        )
        leftovers = response['UnprocessedKeys'].get(table_name, {}).get('Keys', [])
        if "Responses" in response and table_name in response["Responses"]:
            result.extend(response["Responses"][table_name])
    return result


def batch_set_item(table: str, items: list):
    """
    :param items: list of items to set
    """
    dynamo = get_dynamo()
    if not items:
        return
    table = dynamo.Table(table)
    with table.batch_writer() as batch:
        for item in items:
            batch.put_item(Item=item)


def batch_delete_item(table: str, keys: list):
    """
    :param table:
    :param keys: a list of keys list[dict]
    """
    dynamo = get_dynamo()
    if not keys:
        return
    table = dynamo.Table(table)
    with table.batch_writer() as batch:
        for key in keys:
            batch.delete_item(Key=key)


def query_table(table_name: str, attr_name, attr_equals):
    """
    attr_name is the primary key
    :param table_name:
    :param attr_name: the attributes' name
    :param attr_equals: the attributes' value
    :return: items list or []
    """
    dynamo = get_dynamo()
    table = dynamo.Table(table_name)
    response = table.query(
        KeyConditionExpression=Key(attr_name).eq(attr_equals)
    )
    return response['Items']


def scan_table(table_name: str, attr: str, value, limit: int = None, generator: bool = False):
    """
    :param generator: generator to yield results
    :param attr: the attributes' name
    :param value: the attributes' value
    :param limit: how many items to return ? can stay unlimited
    """
    dynamo = get_dynamo()
    table = dynamo.Table(table_name)
    return __scan(table, limit, generator, Attr(attr).eq(value))


def get_all_items(table_name: str):
    """
    :return: all items on the table
    """
    dynamodb = get_dynamo()
    table = dynamodb.Table(table_name)
    return __scan(table, None, False, "")


def create_table(prefix: str, table: str, key: str, key_type: str, range_key: str = None,
                       range_key_type: str = None):
    """
    type: dynamodb type - S, N
    """
    dynamodb = get_dynamo()
    schema = [{"AttributeName": key, "KeyType": "HASH"}]
    attribute_definitions = [{"AttributeName": key, "AttributeType": key_type}]
    if range_key:
        schema.append({"AttributeName": range_key, "KeyType": "RANGE"})
        attribute_definitions.append({"AttributeName": range_key, "AttributeType": range_key_type})
    kwargs = {
        'TableName': f'{prefix}-{table}',
        'KeySchema': schema,
        'AttributeDefinitions': attribute_definitions,
        'BillingMode': 'PAY_PER_REQUEST'
    }
    dynamodb.create_table(**kwargs)
    return f'table_created {prefix}-{table}'


def __scan(table, limit: int, generator: bool, filter_expression: str):
    def generator_func():
        scan_args = {}
        if limit:
            scan_args["Limit"] = limit
        if filter_expression:
            scan_args["FilterExpression"] = filter_expression
        scan = table.scan(**scan_args)
        items = scan['Items']
        yield items
        while 'LastEvaluatedKey' in scan:
            scan_args["ExclusiveStartKey"] = scan["LastEvaluatedKey"]
            scan = table.scan(**scan_args)
            items = scan['Items']
            yield items

    if generator:
        return generator_func
    else:
        return [item for lst in generator_func() for item in lst]
