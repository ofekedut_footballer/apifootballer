import json
import boto3
import base64
from botocore.exceptions import ClientError
from infra import aws

def get_google_recaptcha_secret():
    secret_name = "capcha"
    session = aws.get_session()
    client = session.client(
        service_name='secretsmanager',
    )

    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )
    secret = json.loads(get_secret_value_response['SecretString'])
    return secret['google_recaptcha']
