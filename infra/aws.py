import boto3

def get_session():
    try:
        session = boto3.Session(
            profile_name='footballer',
            region_name='eu-central-1'
            )
    except Exception:
        session = boto3.Session(
            region_name='eu-central-1'
        )
    return session


def get_ses():
    try:
        session = boto3.Session(
            profile_name='footballer',
        )
    except Exception:
        session = boto3.Session(
        )
    client = session.client('ses', 'eu-central-1')
    return client