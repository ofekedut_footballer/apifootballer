import decimal
import os
from datetime import datetime
from infra import dynamodb, recaptcha, S3, ses
import asyncio
import json


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return


class Args:
    def __init__(self):
        self.method: str
        self.route: str = ''
        self.body: dict = {}
        self.prefix = 'op'


class ContextTypes:
    test = 'test'
    operational = 'op'
    record = 'rec'


class ContextManager:
    _instance = None
    _loop = None

    def __init__(self):
        self.args = Args()
        self.recorded_data = {}
        self.type = ContextTypes.operational
        self.tasks = None
        self.result = {}

    def get_recorded_data_filename(self):
        date = datetime.utcnow().date().strftime("%Y_%m_%d")
        if self.args.body.get("test_name"):
            return os.path.join(r'./tests/testfiles',
                                f'{self.args.body["test_name"]}_{date}.json')
        else:
            return os.path.join(r'./tests/testfiles',
                                f'{self.args.route}_{date}.json')


    def save_test(self):
        if self.type == ContextTypes.record:
            file = open(self.get_recorded_data_filename(), "w", encoding='utf8')
            data = {
                'test_data': self.recorded_data,
            }
            data_json = json.dumps(data, indent=4, cls=DecimalEncoder)
            file.write(data_json)


    @classmethod
    def instance(cls):
        if not cls._instance:
            cls._instance = ContextManager()
        if not cls._instance.tasks:
            cls._instance.__init_tasks__()
        return cls._instance

    def __init_tasks__(self):
        if self.type == ContextTypes.operational:
            self.tasks = self.op_tasks()
            # todo tests

    def op_tasks(self):
        return {
            'get_item': dynamodb.get_item,
            'put_item': dynamodb.put_item,
            'batch_delete_item': dynamodb.batch_delete_item,
            'S3_put_obj': S3.pub_obj,
            'scan_table': dynamodb.scan_table,
            'ses_send_email': ses.send_email,
            'date_now': datetime.utcnow().strftime,
            'time_now': datetime.utcnow().strftime,
            'query_table': dynamodb.query_table
        }

    def recaptcha_validation(self, key):
        if self.type == ContextTypes.test or self.type == ContextTypes.record:
            return True
        return recaptcha.validate_recaptcha(key)

    @staticmethod
    def get_dict_from_args(args, kwargs):
        res = {}
        for i, arg in enumerate(args):
            res[str(i)] = arg
        res.update(kwargs)
        res_copy = {}
        for key, item in res.items():
            try:
                res_copy[key] = item.__dict__()
            except:
                res_copy[key] = item
        return res

    async def task(self, task_name, *args, **kwargs):
        if self.type == ContextTypes.operational:
            return self.tasks[task_name](*args, **kwargs)
        elif self.type == ContextTypes.record:
            if task_name not in self.recorded_data:
                self.recorded_data[task_name] = []
            key = self.get_dict_from_args(args, kwargs)
            val = self.tasks[task_name](*args, **kwargs)
            item = {
                'key': key,
                'val': val
            }
            self.recorded_data[task_name].append(item)
            return val

    def get_loop(self):
        if self._loop:
            return self._loop
        else:
            self._loop = asyncio.new_event_loop()
            asyncio.set_event_loop(self._loop)
            return self._loop

    def date_now(self):
        return self.get_loop().run_until_complete(*[self.task('date_now', "%d/%m/%Y")])

    def time_now(self):
        return self.get_loop().run_until_complete(*[self.task('time_now', "%d/%m/%Y %H:%M:%S")])

    async def get_item(self, table: str, key: dict):
        return await self.task('get_item', table, key)

    async def put_item(self, table: str, item: dict):
        return await self.task('put_item', table, item)

    async def query_item(self, table: str, key, key_value):
        return await self.task('query_table', table, key, key_value)

    async def batch_delete_item(self, table: str, keys: list):
        return await self.task('batch_delete_item', table, keys)

    async def S3_put_obj(self, bucket_name: str, obj_name: str, data: dict):
        return await self.task('S3_put_obj', bucket_name, obj_name, data)

    async def scan_table(self, table_name: str, attr: str, value, limit: int = None, generator: bool = False):
        return await self.task('scan_table', table_name, attr, value, limit, generator)

    async def ses_send_email(self, recepient: str, subject: str, body_html, body_reg, verification_print: bool = False):
        return await self.task('ses_send_email', recepient, subject, body_html, body_reg, verification_print)
