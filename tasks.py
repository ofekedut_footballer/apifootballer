from context_managment import Args
import http_statuses, players, clubs, scouts
import scouts.logger as scouts_logger
import clubs.logger as clubs_logger
from clubs.tasks import approve_club
from players.tasks import approve_player
from scouts.tasks import approve_scouter
import players.logger as players_logger
from scouts.tasks.approve_scouter import waiting_approval

def  login(args: Args):
    login_types = {
        "club": clubs.logger.login,
        "scout": scouts.logger.login,
        "player": players.logger.login
    }
    body = args.body
    prefix = args.prefix
    type = body.get('type')
    return login_types[type](body, prefix) if type else http_statuses.missing_argument('type')


def new_scout(args: Args):
    body = args.body
    prefix = args.prefix
    res = scouts_logger.singup(body, prefix)
    return res


def new_club(args: Args):
    body = args.body
    prefix = args.prefix
    res = clubs_logger.signup(body, prefix)
    return res


def new_player(args: Args):
    body = args.body
    prefix = args.prefix
    res = players_logger.singup(body, prefix)
    return res


def scouters_waiting_approval(args: Args):
    prefix = args.prefix
    return approve_scouter.waiting_approval(prefix)


def clubs_waiting_approval(args: Args):
    prefix = args.prefix
    return approve_club.waiting_approval(prefix)

def player_waiting_approval(args: Args):
    prefix = args.prefix
    return approve_player.waiting_approval(prefix)

def approve_scouters(args: Args):
    prefix = args.prefix
    body = args.body
    id = body.get('id')
    return approve_scouter.approve_scouter(prefix, id)


def approve_clubs(args: Args):
    prefix = args.prefix
    body = args.body
    id = body.get('id')
    return approve_club.approve_club(prefix, id)

def approve_players(args: Args):
    prefix = args.prefix
    body = args.body
    id = body.get('id')
    return approve_player.approve_player(prefix, id)

def registration_files_uploaded(args: Args):
    prefix = args.prefix
    body = args.body
    id = body.get('id')
    type = id[0]
    cases = {
        'S': scouts.logger.files_uploaded,
        'C': clubs.logger.files_uploaded,
        'P': players.logger.files_uploaded
    }
    return cases[type](prefix, id) if type in cases else http_statuses.invalid_form()


