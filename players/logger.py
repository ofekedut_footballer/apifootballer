import asyncio, general_info
from random import randint
import context_managment, http_statuses, passwords
from players.player import Player, PlayerStatuses, Custodian
from datetime import datetime, timedelta
from dtb.token_email import TokenEmailTable
from dtb.id_email import IdEmailTable
from dtb.email_id import EmailIdTable
from uuid import uuid4
import emails

cm = context_managment.ContextManager.instance()


def generate_id(prefix):
    def rand_num():
        return randint(10000000, 99999999)

    loop = cm.get_loop()
    num = rand_num()
    req = [
        cm.get_item(f'{prefix}-id-email', {'id': 'P' + str(num)})
    ]
    id = loop.run_until_complete(asyncio.gather(*req))
    while id:
        num = rand_num()
        req = [
            cm.get_item(f'{prefix}-id-email', {'id': 'P' + str(num)})
        ]
        id = loop.run_until_complete(asyncio.gather(*req))[0]
    return 'P' + str(num)


def singup_step1(args: context_managment.Args):
    body = args.body
    prefix = args.prefix
    recaptcha = body.get("recaptcha_token")
    if not recaptcha:
        return http_statuses.missing_argument('RECAPTCHA')
    if not cm.recaptcha_validation(recaptcha):
        return http_statuses.invalid_recaptcha()

    user_data = body.get('data')
    if not user_data:
        return http_statuses.missing_argument('data')
    for key in ['tos', 'pp']:
        if not user_data.get(key):
            return http_statuses.invalid_form_field(key)

    player = Player()
    for key in ['first_name', 'middle_name', 'last_name', 'gender', 'email', 'birthday']:
        error = player.set_attr(key, user_data.get(key))
        if error:
            return error

    user_given_password = user_data.get('password')
    if not user_given_password:
        return http_statuses.missing_argument('password')
    if not passwords.validate_syntax(user_given_password):
        return http_statuses.invalid_form_field('password')

    salt = passwords.generate_salt()
    password_encrypted = passwords.encrypt_password(salt, user_given_password)
    id = generate_id(prefix)
    user_data['added'] = cm.time_now()
    user_data['id'] = id
    user_data['locale'] = 'en'
    user_data['status'] = PlayerStatuses.WAITING_EMAIL_APPROVAL
    user_data['under_18'] = player.calculate_age() < 18
    user_password = {
        'id': id,
        'password': password_encrypted
    }
    user_salt = {
        'id': id,
        'salt': salt
    }
    token = uuid4().hex[:32]
    passwords.add_password(prefix, user_password, user_salt)
    now = cm.time_now()
    current_time = datetime.strptime(now, "%d/%m/%Y %H:%M:%S")
    unix_timestamp = (now + timedelta(days=365)).timestamp()
    req = [
        cm.put_item(f'{prefix}-player-signup-info', user_data),
        cm.put_item(f'{prefix}-id-email', {'id': id, 'email': user_data['email']}),
        cm.put_item(f'{prefix}-email-id', {'id': id, 'email': user_data['email']}),
        TokenEmailTable.put_token(prefix, token, user_data['email'], id, int(unix_timestamp))
    ]
    res = cm.get_loop().run_until_complete(asyncio.gather(*req))
    html_text = f"""
    <h1>demo email</h1>
    <br>
    <p>https://localhost:3000/verifyEmail?token={token}</p>
    """
    text = f"""demo email
    https://localhost:3000/verifyEmail?token={token}
    """
    cm.get_loop().run_until_complete(*[cm.ses_send_email(user_data['email'], 'validation_mail', html_text, text, True)])
    return http_statuses.success_form()


def signup_approve_email(args: context_managment.Args):
    body = args.body
    prefix = args.prefix
    token = body.get('token')
    if not token:
        return http_statuses.missing_argument('token')
    email = cm.get_loop().run_until_complete(*[TokenEmailTable.get_email(prefix, token), ])
    if not email:
        return http_statuses.invalid_form_field('token')
    id, token = cm.get_loop().run_until_complete(
        asyncio.gather(*[EmailIdTable.get_id(prefix, email),
                         TokenEmailTable.delete_token(prefix, token)]))
    user_data = cm.get_loop().run_until_complete(*[cm.get_item(f'{prefix}-player-signup-info', {'id': id})])
    user_data['status'] = PlayerStatuses.EMAIL_APPROVED
    cm.get_loop().run_until_complete(asyncio.gather(*[cm.put_item(f'{prefix}-player-signup-info', user_data)]))
    return {
        **http_statuses.success(), 'id': id, 'email': email, 'minor': user_data['under_18'],
    }


def signup_step2(args: context_managment.Args):
    body = args.body
    prefix = args.prefix
    data = body.get('data')
    if not data:
        return http_statuses.missing_argument('data')
    player = Player()
    for key in ['id', 'phone', 'country', 'city', 'address', "zip_code", 'agency_status', 'agency_expire',
                'club_expire',
                'current_club', 'contract_expire', 'salary', 'primary_position', 'secondary_position', 'description',
                'passports_count', 'passport_id']:
        error = player.set_attr(key, data.get(key))
        if error:
            return error
    info = cm.get_loop().run_until_complete(*[cm.get_item(f'{prefix}-player-signup-info', {'id': data['id']})])
    info = {**info, **data}
    info['status'] = PlayerStatuses.CUSTODIAN_REQUIRED if info['under_18'] else PlayerStatuses.WAITING_APPROVAL
    cm.get_loop().run_until_complete(*[cm.put_item(f'{prefix}-player-signup-info', info)])
    return http_statuses.success_form()


def singup_custodian(args: context_managment.Args):
    body = args.body
    prefix = args.prefix
    data = body.get('data')
    if not data:
        return http_statuses.missing_argument('data')
    custodian = Custodian()
    for key in ['first_name', 'middle_name', 'last_name', 'email', 'birthday', 'phone', 'relation', 'passport_id']:
        error = custodian.set_attr(key, data.get(key))
        if error:
            return error
    id = data.get('id')
    if not id:
        return http_statuses.missing_argument('id')
    info = cm.get_loop().run_until_complete(*[cm.get_item(f'{prefix}-player-signup-info', {'id': id})])
    info = {**info, **data}
    cm.get_loop().run_until_complete(*[cm.put_item(f'{prefix}-player-signup-info', info)])
    return http_statuses.success_form()


def files_uploaded(prefix, id):
    player = Player()
    added = player.set_attr('id', id)
    if added:
        return added
    loop = cm.get_loop()
    req = [
        cm.__get_item(f'{prefix}-player-signup-info', {'id': id}),
    ]
    info = loop.run_until_complete(asyncio.gather(*req))[0]
    info['status'] = 'waiting_approval'
    req = {
        cm.put_item(f'{prefix}-player-signup-info', info)
    }
    res = loop.run_until_complete(*req)
    return http_statuses.success()


def login(body: dict, prefix: str):
    username = body.get('username')
    password = body.get('password')
    player = Player()
    player.password = password
    player.id = username
    loop = cm.get_loop()
    asyncio.set_event_loop(loop)
    req = [
        cm.__get_item(f'{prefix}-user-salt', {'id': player.id}),
        cm.__get_item(f'{prefix}-user-passwords', {'id': player.id}),
        cm.__get_item(f'{prefix}-player-signup-info', {'id': player.id})
    ]
    salt, saved_pass, info = loop.run_until_complete(asyncio.gather(*req))
    salt = salt.get('salt')
    saved_pass = saved_pass.get('password')
    encrypted = passwords.encrypt_password(salt, player.password)
    if encrypted == saved_pass:
        return {**http_statuses.success_login_club(), **info}
    return http_statuses.invalid_password()


if __name__ == '__main__':
    item = {
        'data': {
            'first_name': 'ofek', 'middle_name': 'rgadf', 'last_name': 'edut', 'gender': 'female',
            'email': 'ofekedut86@gmail.com', 'birthday': '04/12/1998', 'tos': True, 'pp': True,
            'password': passwords.generate_password()
        }
    }
    tokendata = {
        'token': '98603b886b014a969c07ff3539fd0cb1'
    }
    item2 = {
        'data': {'id': 'P72024866', 'phone': '+972 0584300111', 'country': "israel", 'city': 'kgc', 'address': 'street',
                 "zip_code": '8205001', 'agency_status': 'active', 'agency_expire': '04/12/1998',
                 'club_expire': "04/12/1998",
                 'current_club': 'asf', 'contract_expire': '04/12/1998', 'salary': 'milions', 'primary_position': 'AM',
                 'secondary_position': 'AM',
                 'description': 'meleh ha golim',
                 'passports_count': 2, 'passport_id': '123213213123'}}
    item3 = {'data': {
        'first_name': 'aba',
        'middle_name': '',
        'last_name': "edut",
        'email': 'aba@gmail.com',
        'birthday': '04/12/1998',
        'phone': '+972 0584300111',
        'relation': 'guardian',
        'passport_id': '123123123'
    }}
    print(singup_custodian(item3, 'op'))
