import re
from datetime import date, datetime

import emails
import http_statuses


class PlayerStatuses:
    WAITING_APPROVAL = 'waiting_approval'
    CUSTODIAN_REQUIRED = 'custodian_required'
    EMAIL_APPROVED = 'email_approved'
    WAITING_EMAIL_APPROVAL = 'waiting_email_approval'


class Phone:
    def __init__(self):
        self.region: str
        self.number: str


class Custodian:
    def __init__(self):
        self.first_name: str = ''
        self.middle_name: str = ''
        self.last_name: str = ''
        self.birthday: str = ''
        self.passport: str = ''
        self.phone = Phone()
        self.required = False

    def set_attr(self, key, val):

        def set_name():
            if key == 'middle_name' and not val:
                return
            if not val:
                return http_statuses.missing_argument(key)
            if key == 'first_name' and len(val) < 2:
                return http_statuses.invalid_form_field(key)
            if len(val) > 30:
                return http_statuses.invalid_form_field(key)
            self.__setattr__(key, val)
            return

        def set_birthday():
            if re.match(r'\d\d/\d\d/\d\d\d\d', val):
                self.__setattr__(key, val)
                return
            return http_statuses.invalid_form_field(key)

        def set_passport():
            if not re.compile(r'^[\d]+$', val):
                return http_statuses.invalid_form_field(key)
            self.__setattr__(key, val)
            return

        def set_phone():
            phone = re.split(r'[\s]+', val)
            if len(phone) != 2:
                return http_statuses.invalid_form_field(key)
            region = phone[0]
            number = phone[1]
            if region[0] != '+':
                return http_statuses.invalid_form_field(key)
            if not re.match(r'^[\d]+$', number):
                return http_statuses.invalid_form_field(key)
            self.phone.region = region
            self.phone.number = number
            return

        def set_relation():
            if not val:
                return http_statuses.missing_argument(key)
            if val not in ['parent', 'guardian', 'other']:
                return http_statuses.invalid_form_field(key)
            self.__setattr__(key, val)
            return

        cases = {
            "first_name": set_name,
            "middle_name": set_name,
            "last_name": set_name,
            "birthday": set_birthday,
            "passport_id": set_passport,
            "phone": set_phone,
            'relation': set_relation,
            'email': set_email
        }

        if key in cases:
            return cases[key]()
        return http_statuses.invalid_form_field(key)


class Positions:
    def __init__(self):
        self.TM = "Target Man"
        self.FN = "False 9"
        self.IFl = "Inside Forward Left"
        self.IFr = "Inside Forward Right"
        self.APfl = "Advance Playmaker front left"
        self.APT = "AP(10)"
        self.SS = "Shadow Striker"
        self.APbl = "Advance Playmaker back left"
        self.WSl = "wing forward left"
        self.WSr = "Stay Wide 50 right"  # StayWide50/50
        self.LDW = "Midfielder Left"
        self.BBM = "Box To Box Midfielder"  # 50/50(8)
        self.RDW = "Midfielder right"
        self.WBL = "Wing Back left"
        self.AM = "Anchor Man"
        self.DPL = "Deepline Playmaker left"
        self.WBR = "Wing Back right"
        self.FBL = "Full Back left"
        self.FBR = "Full Back right"
        self.CB = "Center Breaker"
        self.LIBERO = "Libero"
        self.BPD = "Ball Playing Maker"  # BallPlayingDefender
        self.GK = "Goal Keeper"

    def names(self):
        return list(self.__dict__.values())

    def poses(self):
        return list(self.__dict__.keys())

    def poses_to_name(self):
        return dict(self.__dict__)


class Player:
    def __init__(self):
        self.name: str = ''
        self.password: str = ''
        self.id: str = ''
        self.email: str = ''
        self.phone = Phone()
        self.birthday: str = ''
        self.custodian = Custodian()

    def calculate_age(self):
        born = datetime.strptime(self.birthday, "%d/%m/%Y")
        today = datetime.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    @classmethod
    def validate_id_syntax(cls, id):
        if not id:
            return http_statuses.invalid_form_field(id)
        if id[0] != 'P':
            return http_statuses.invalid_player_id()
        if not re.match(r'^[0-9]+$', id[1:]):
            return http_statuses.invalid_player_id()
        return

    def set_name(self, key, val):
        if not val:
            http_statuses.missing_argument(key)
        if len(key) > 30:
            return http_statuses.invalid_form_field(val)
        if key == 'first_name' and len(val) < 2:
            return http_statuses.invalid_form_field(val)
        self.__setattr__(key, val)
        return

    def set_id(self, key, val):
        if not val:
            return http_statuses.missing_argument(key)
        if self.validate_id_syntax(val):
            return http_statuses.invalid_player_id()
        self.__setattr__(key, val)

    def set_birthday(self, key, val):
        if re.match(r'\d\d/\d\d/\d\d\d\d', val):
            self.__setattr__(key, val)
            return
        return http_statuses.invalid_form_field(key)

    def set_email(self, key, val):
        if not emails.validate_email(val):
            return http_statuses.invalid_form_field(key)
        self.__setattr__(key, val)
        return

    def set_phone(self, key, val):
        phone = re.split(r'[\s]+', val)
        if len(phone) != 2:
            return http_statuses.invalid_form_field(key)
        region = phone[0]
        number = phone[1]
        if region[0] != '+':
            return http_statuses.invalid_form_field(key)
        if not re.match(r'^[\d]+$', number):
            return http_statuses.invalid_form_field(key)
        self.phone.region = region
        self.phone.number = number
        return

    def set_passport_count(self, key, val):
        if not re.match(r'[\d]+', str(val)):
            return http_statuses.missing_argument(key)
        self.__setattr__(key, int(val))
        return

    def set_passport(self, key, val):
        if not val:
            return http_statuses.missing_argument(key)
        self.__setattr__(key, int(val))
        return

    def set_position(self, key, val):
        poses = Positions().poses()
        if not val:
            return http_statuses.missing_argument(key)
        if val not in poses:
            return http_statuses.invalid_form_field(key)
        self.__setattr__(key, val)

    def set_gender(self, key, val):
        if not val:
            return http_statuses.missing_argument(key)
        if val.lower() not in ['male', 'female', 'other']:
            return http_statuses.invalid_form_field(key)
        self.__setattr__(key, val)

    def set_attr(self, key, val):
        cases = {

            'gender': self.set_gender,
            'id': self.set_id,
            'first_name': self.set_name,
            'middle_name': self.set_name,
            'last_name': self.set_name,
            'birthday': self.set_birthday,
            'email': self.set_email,
            'phone': self.set_phone,
            'country': self.__setattr__,
            'city': self.__setattr__,
            'address': self.__setattr__,
            'passports_count': self.set_passport_count,
            'passport_id_number': self.set_passport,
            'agency_status': self.__setattr__,
            'agency_expire': self.__setattr__,
            'current_club': self.__setattr__,
            'contract_expire': self.__setattr__,
            'salary': self.__setattr__,
            'primary_position': self.set_position,
            'secondary_position': self.set_position,
            'description': self.__setattr__}

        if key in cases:
            return cases[key](key, val)
        else:
            self.__setattr__(key, val)
            return


if __name__ == "__main__":
    a = ['first_name', 'middle_name', 'last_name', 'birthday', 'email', 'phone', 'country',
         'city', 'address', 'passports_count', 'passport_id_number', 'agency_status', 'agency_expire',
         'current_club', 'contract_expire', 'salary', 'primary_position', 'secondary_position', 'description', 'tos',
         'pp']
    b = ''
    for key in a:
        b += f'\"{key}\": \n'
    print(b)
