import asyncio

import context_managment, http_statuses
import emails
cm = context_managment.ContextManager.instance()


def email_to_id(email: str, prefix):
    req = [
        cm.__get_item(f'{prefix}-email-id', {'email': email})
    ]
    id = cm.get_loop().run_until_complete(asyncio.gather(*req))
    if not id:
        return False
    id = id[0]['id']
    return id