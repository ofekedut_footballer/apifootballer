import context_managment
import http_statuses
from dtb import email_id, email_token, token_email
import datetime, uuid, emails, asyncio
import passwords

cm = context_managment.ContextManager.instance()


def reset_pass_email(args: context_managment.Args):
    body = args.body
    prefix = args.prefix
    for key in ['recaptcha', 'data']:
        if key not in body:
            return http_statuses.missing_argument(key)
    recaptcha = body.get('recaptcha')
    data = body.get('data')
    if 'email' not in data:
        return http_statuses.missing_argument('email')
    if not cm.recaptcha_validation(recaptcha):
        return http_statuses.invalid_recaptcha()
    id = cm.get_loop().run_until_complete(email_id.EmailIdTable.get_id(prefix, data['email']))
    if not id:
        return http_statuses.invalid_email()
    time_now = cm.time_now()
    current_time = datetime.datetime.strptime(time_now, "%d/%m/%Y %H:%M:%S")
    unix_timestamp = current_time.timestamp()
    unix_timestamp_in_20_min = unix_timestamp + (20 * 60)  # 20 min * 60 seconds
    token = uuid.uuid4().hex[:32]
    text = f"""
        https://localhost:3000/resetPassword?token={token}
    """
    html_text = f"""
            <p>https://localhost:3000/resetPassword?token={token}</p>
    """
    cm.get_loop().run_until_complete(asyncio.gather(
        *[email_token.EmailTokenTable.set_token(prefix, data['email'], token, id, int(unix_timestamp_in_20_min)),
          token_email.TokenEmailTable.put_token(prefix, token, data['email'], id, int(unix_timestamp_in_20_min)),
          cm.ses_send_email(data['email'], 'reset password footballer', html_text, text)]))
    return http_statuses.success()


def reset_validate_token(args: context_managment.Args):
    body = args.body
    prefix = args.prefix
    if 'data' not in body:
        return http_statuses.missing_argument('data')
    data = body.get('data')
    if 'token' not in data:
        return http_statuses.missing_argument('token')
    token = data.get('token')
    email = cm.get_loop().run_until_complete(*[token_email.TokenEmailTable.get_email(prefix, token)])
    if not email:
        return http_statuses.invalid_token()
    return http_statuses.valid_token()


def reset_pass_new(args: context_managment.Args):
    body = args.body
    prefix = args.prefix
    valid = reset_validate_token(args)
    if valid != http_statuses.valid_token():
        return valid
    data = body['data']
    password = data.get('password')
    token = data.get('token')
    if not password:
        return http_statuses.missing_argument('password')
    if not token:
        return http_statuses.missing_argument('token')
    if not passwords.validate_syntax(password):
        return http_statuses.invalid_password()
    salt = passwords.generate_salt()
    encrypted = passwords.encrypt_password(salt, password)
    email = cm.get_loop().run_until_complete(*[token_email.TokenEmailTable.get_email(prefix, token)])
    id = cm.get_loop().run_until_complete(*[email_id.EmailIdTable.get_id(prefix, email)])
    user_password = {
        'id': id,
        'password': encrypted
    }
    user_salt = {
        'id': id,
        'salt': salt
    }
    passwords.add_password(prefix, user_password, user_salt)
    cm.get_loop().run_until_complete(*[email_token.EmailTokenTable.delete_token(prefix, email),
                                       token_email.TokenEmailTable.delete_token(prefix, token)])
    return http_statuses.success()

if __name__ == "__main__":
    time_now = cm.time_now()
    current_time = datetime.datetime.strptime(time_now, "%d/%m/%Y %H:%M:%S")
    unix_timestamp = current_time.timestamp()
    unix_timestamp_in_20_min = unix_timestamp + (20 * 60)  # 20 min * 60 seconds
    token = uuid.uuid4().hex[:32]
    print(token)