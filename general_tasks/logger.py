import asyncio, uuid, email, context_managment, http_statuses, passwords
from datetime import datetime, timedelta
from random import randint
from utils import converters
from clubs import logger as clubs_logger
from players import logger as players_logger
from scouts import logger as scouts_logger

cm = context_managment.ContextManager.instance()


def login(body: dict, prefix: str) -> dict:
    username = body.get('username')
    password = body.get('password')
    if not username:
        return http_statuses.missing_argument('username')
    if not password:
        return http_statuses.missing_argument('password')

    type = 'E' if '@' in username else username[0]
    if type == 'E':
        username = converters.email_to_id(username, prefix)
        if not id:
            return http_statuses.invalid_email()
        type = username[0]
    cases = {'C': clubs_logger.login, 'S': scouts_logger.login, 'P': players_logger.login}
    if type not in cases:
        return http_statuses.invalid_form_field('id')
    data = {
        'username': username,
        'password': password
    }
    return cases[type](data, prefix)
