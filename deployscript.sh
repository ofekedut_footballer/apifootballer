#!/bin/bash
aws lambda publish-version --profile footballer --function-name database_api  --region "eu-central-1"
aws lambda update-function-code --profile footballer --function-name database_api --zip-file fileb://./archive.zip  --region "eu-central-1"
rm -rf archive.zip
